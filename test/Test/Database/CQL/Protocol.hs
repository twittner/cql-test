-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at http://mozilla.org/MPL/2.0/.

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeFamilies      #-}

module Test.Database.CQL.Protocol where

import Control.Applicative
import Control.Monad
import Control.Monad.Identity
import Control.Monad.IO.Class
import Data.Decimal
import Data.Int
import Data.Maybe
import Data.Text (Text)
import Data.Time
import Data.UUID
import Test.Tasty
import Test.Tasty.HUnit
import Database.CQL.Protocol
import Database.CQL.IO

import qualified Data.Set as S

data Rec = Rec
    { ra :: Int64
    , rb :: Ascii
    , rc :: Blob
    , rd :: Bool
    , rf :: Decimal
    , rg :: Double
    , rh :: Float
    , ri :: Int32
    , rj :: UTCTime
    , rk :: UUID
    , rl :: Text
    , rm :: Integer
    , rn :: TimeUuid
    , ro :: Inet
    , rp :: [Int32]
    , rq :: Set Ascii
    , rr :: Map Ascii Int32
    , rs :: Maybe Int32
    } deriving (Show)

recordInstance ''Rec

tests :: Pool -> TestTree
tests p = testGroup "Request"
    [ testCase "keyspace" (void $ runClient p createKeyspace)
    , testCase "table"    (void $ runClient p createTable)
    , testCase "insert"   (runClient p insertTable)
    , testCase "query"    (void $ runClient p queryRequest)
    , testCase "prepare"  (void $ runClient p prepareRequest)
    , testCase "batch"    (runClient p batchRequest)
    , testCase "drop"     (void $ runClient p dropKeyspace)
    ]

createKeyspace :: Client ()
createKeyspace = void $
    schema "create keyspace cqltest with replication = { 'class': 'SimpleStrategy', 'replication_factor': '3' }"
           (params ())

dropKeyspace :: Client ()
dropKeyspace = void $
    schema "drop keyspace cqltest"
           (params ())

queryRequest :: Client [(Text, Bool, Text, Text)]
queryRequest =
    query "select * from system.schema_keyspaces where keyspace_name = ?"
          (params (Identity ("system" :: Ascii)))

prepareRequest :: Client [Identity Ascii]
prepareRequest = do
    qid <- prepare s
    execute qid (params (Identity 483563763861853456384))
  where
    s :: QueryString R (Identity Int64) (Identity Ascii)
    s = "select b from cqltest.test where a = ?"

batchRequest :: Client ()
batchRequest = batch One BatchUnLogged
    [ BatchQuery (QueryString "insert into cqltest.test (a, b) values (?, ?)" :: QueryString W (Int64, Ascii) ()) (42, "xxxx")
    , BatchQuery (QueryString "insert into cqltest.test (a, b) values (?, ?)" :: QueryString W (Int64, Ascii) ()) (43, "yyyy")
    ]

createTable :: Client ()
createTable = void $
    schema "create columnfamily cqltest.test (\
        \  a bigint\
        \, b ascii\
        \, c blob\
        \, d boolean\
        \, f decimal\
        \, g double\
        \, h float\
        \, i int\
        \, j timestamp\
        \, k uuid\
        \, l varchar\
        \, m varint\
        \, n timeuuid\
        \, o inet\
        \, p list<int>\
        \, q set<ascii>\
        \, r map<ascii,int>\
        \, s int\
        \, primary key (a)\
        \)"
        (params ())

insertTable :: Client ()
insertTable = do
    t <- liftIO getCurrentTime
    let r = Rec
            { ra = 483563763861853456384
            , rb = "hello world"
            , rc = Blob "blooooooooooooooooooooooob"
            , rd = False
            , rf = 1.2342342342423423423423423442
            , rg = 433243.13
            , rh = 1.23
            , ri = 2342342
            , rj = t
            , rk = fromJust . fromString $ "af93aafe-dea5-4427-bea4-8d7872507efb"
            , rl = "sdfsdžȢぴせそぼξλж҈Ҵאבג"
            , rm = 8763847563478568734687345683765873458734
            , rn = TimeUuid . fromJust $ fromString "559ab19e-52d8-11e3-a847-270bf6910c08"
            , ro = Inet4 0X7F00001
            , rp = [1,2,3]
            , rq = Set ["peter", "paul", "mary"]
            , rr = Map [("peter", 1), ("paul", 2), ("mary", 3)]
            , rs = Just 42
            }
    void $ insert r
    x <- map asRecord <$> select ()
    liftIO $ do
        1    @=? length x
        ra r @=? ra (head x)
        rb r @=? rb (head x)
        rc r @=? rc (head x)
        rd r @=? rd (head x)
        rf r @=? rf (head x)
        rg r @=? rg (head x)
        rh r @=? rh (head x)
        ri r @=? ri (head x)
        --rj r @=? rj (head x)
        rk r @=? rk (head x)
        rl r @=? rl (head x)
        rm r @=? rm (head x)
        rn r @=? rn (head x)
        ro r @=? ro (head x)
        rp r @=? rp (head x)
        case (rq r, rq (head x)) of
            (Set a, Set b) -> S.fromList a @=? S.fromList b
        case (rr r, rr (head x)) of
            (Map a, Map b) -> S.fromList a @=? S.fromList b
        rs r @=? rs (head x)
  where
    insert = write "insert into cqltest.test (a,b,c,d,f,g,h,i,j,k,l,m,n,o,p,q,r,s) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" . params . asTuple
    select = query "select a,b,c,d,f,g,h,i,j,k,l,m,n,o,p,q,r,s from cqltest.test" . params

params :: (Tuple a) => a -> QueryParams a
params p = (QueryParams One False p Nothing Nothing Nothing)
